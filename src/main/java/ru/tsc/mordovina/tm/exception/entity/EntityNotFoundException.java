package ru.tsc.mordovina.tm.exception.entity;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class EntityNotFoundException extends AbstractException {

    public EntityNotFoundException() {
        super("Error. Entity not found.");
    }

}
