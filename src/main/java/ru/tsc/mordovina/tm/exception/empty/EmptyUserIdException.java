package ru.tsc.mordovina.tm.exception.empty;

import ru.tsc.mordovina.tm.exception.AbstractException;

public class EmptyUserIdException extends AbstractException {

    public EmptyUserIdException() {
        super("Error. User id is empty.");
    }

}
