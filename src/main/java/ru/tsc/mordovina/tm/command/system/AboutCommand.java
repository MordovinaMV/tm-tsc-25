package ru.tsc.mordovina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "about";
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-a";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display developer info";
    }

    @Override
    public void execute() {
        System.out.println("Developer: " + serviceLocator.getPropertyService().getDeveloperName());
        System.out.println("e-mail: " + serviceLocator.getPropertyService().getDeveloperEmail());
    }

}
