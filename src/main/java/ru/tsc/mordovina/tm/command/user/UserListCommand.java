package ru.tsc.mordovina.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractUserCommand;
import ru.tsc.mordovina.tm.enumerated.Role;
import ru.tsc.mordovina.tm.exception.empty.EmptyUserListException;
import ru.tsc.mordovina.tm.model.User;

import java.util.List;

public final class UserListCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

    @NotNull
    @Override
    public String getCommand() {
        return "user-list";
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display list of users";
    }

    @Override
    public void execute() {
        System.out.println("User list");
        @Nullable final List<User> users = serviceLocator.getUserService().findAll();
        if (null == users) throw new EmptyUserListException();
        for (final User user : users)
            showUser(user);
    }

}
