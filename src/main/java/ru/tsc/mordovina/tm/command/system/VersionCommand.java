package ru.tsc.mordovina.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.mordovina.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getCommand() {
        return "version";
    }

    @Nullable
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Display program version";
    }

    @Override
    public void execute() {
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }

}
